import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Client extends Thread implements ActionListener{
	private SenderUtil sender;
	private JFrame frame = new JFrame();
	private JTextArea toUser;
	private JButton sendButton, settingsButton;
	private JScrollPane qScroller;
	private JPanel mainPanel;
	private JTextField fromUser;
	private SpringLayout layout;
	private String username, ip;
	private Settings settings;
	private Socket clientSocket;
	private int port;
	private static final String CODE = "\u00A7";
	private static final int OK = 0;
	private static final int GENERIC_ERROR = 2;
	private static final int VERSION_NOT_EXISTS = 3;
	private static final int MESSAGE = 7;
	private static final int USER_JOIN = 9;
	private static final int USER_LEAVE = 10;
	Client(){
		layout = new SpringLayout();
		fromUser = new JTextField(35);
		mainPanel = new JPanel();
		settingsButton = new JButton();
		toUser = new JTextArea(15,40);
		qScroller = new JScrollPane(toUser);
		sendButton = new JButton("Send");
		start();
	}
	public void run(){
		username = "dummy";
		ip = "192.168.1.51";
		port = 12556;
		settings = new Settings(this);
		int version = 2;
		String message;
		try{
			Image img = ImageIO.read(getClass().getResource("ic_settings_black_18dp_1x.png"));
			settingsButton.setIcon(new ImageIcon(img));
			settingsButton.setSize(10,10);
			mainPanel.setLayout(layout);
			toUser.setLineWrap(true);
			toUser.setWrapStyleWord(true);
			toUser.setEditable(false);
			qScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS) ;
			qScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			sendButton.addActionListener(this);
			mainPanel.add(settingsButton);
			mainPanel.add(qScroller) ;
			mainPanel.add(fromUser);
			mainPanel.add(sendButton);
			settingsButton.addActionListener(this);
			//constraint layout
			layout.putConstraint(SpringLayout.NORTH, qScroller, 25,SpringLayout.NORTH, mainPanel);
			layout.putConstraint(SpringLayout.NORTH, fromUser, 5,SpringLayout.SOUTH, qScroller);
			layout.putConstraint(SpringLayout.NORTH, sendButton, 5,SpringLayout.SOUTH, qScroller);

			layout.putConstraint(SpringLayout.EAST, settingsButton,0,SpringLayout.EAST, mainPanel);
			layout.putConstraint(SpringLayout.EAST, sendButton, -5, SpringLayout.EAST, mainPanel);

			layout.putConstraint(SpringLayout.WEST, qScroller, 5, SpringLayout.WEST, mainPanel);
			layout.putConstraint(SpringLayout.WEST, fromUser, 5, SpringLayout.WEST, mainPanel);

			frame.setTitle("Client - TChatP");
			frame.getContentPane().add(mainPanel,BorderLayout.CENTER);
			frame.setSize(470,320);
			frame.setResizable(false);
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);
			clientSocket = new Socket(ip, port);
			frame.addWindowListener(new WindowAdapter(){
				@Override
				public void windowClosing(WindowEvent windowEvent){
					sender.send(USER_LEAVE+CODE);
					System.exit(0);
				}
			});
			BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			sender = new SenderUtil(clientSocket, version,this);
			while(true){
				if((message = inFromServer.readLine()) != null) {
					System.out.println(message);
					if(message.contains(CODE)){
						int typeOfMessage = Integer.parseInt(message.split(CODE)[0]);
						switch(typeOfMessage){
							case OK:
								display("Connected Users:\n");
								for(int i = 1; i<message.split(CODE).length;i++){
									display(message.split(CODE)[i] + "\n");
								}
								break;
							case USER_LEAVE:
								display(message.split(CODE)[1]+" disconnected.\n");
								break;
							case USER_JOIN:
								display(message.split(CODE)[1]+" joined the chat.\n");
								break;
							case MESSAGE:
								display(message.split(CODE)[2]+":"+message.split(CODE)[1]);
								break;
							case GENERIC_ERROR:
								JOptionPane.showMessageDialog(null, message.split(CODE)[1]);
								System.exit(0);
								break;
						}
					}else{
						int typeOfMessage = Integer.parseInt(message.split(CODE)[0]);
						switch(typeOfMessage){
							case OK:
								display("Your version is up to date.\n");
								break;
							case VERSION_NOT_EXISTS:
								JOptionPane.showMessageDialog(null, "This version is newer, this shouldn't happen!");
								System.exit(0);
								break;
						}
					}
				}
			}
		}catch(Exception ignored){}
	}
	private void display(String newText){
		toUser.setText(toUser.getText() + newText);
	}
	public void actionPerformed(ActionEvent ev){
		if(ev.getSource() == sendButton){//Send Button
			send(fromUser.getText());
			fromUser.setText(null);
		}else if(ev.getSource() == settingsButton){//Settings Button
			settings.setVisible(true);
		}
	}
	
	// GETTERS AND SETTERS
	
	private void send(String message){
		sender.send(MESSAGE + CODE + message + CODE + username);
	}
	public String getUsername(){
		return username;
	}
	public void setUsername(String newUsername){
		username = newUsername;
	}
	public String getIp(){
		return ip;
	}
	public void setIp(String newIp){
		ip = newIp;
	}
	public int getPort(){
		return port;
	}
	public void setPort(int newPort){
		port = newPort;
	}
}
