import javax.swing.*;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server extends Thread{
	int port = 12556;
	int correctVersion = 2;
	private ArrayList<Socket> sockets;
	private ServerSocket serverSocket;
	private JFrame frame;
	private JTextArea toUser;
	private JScrollPane qScroller;
	private JPanel mainPanel;
	private ArrayList<String> userList;
	public Server(){
		userList = new ArrayList<>();
		sockets = new ArrayList<>();
		frame = new JFrame();
		toUser = new JTextArea(22,31);
		qScroller = new JScrollPane(toUser);
		mainPanel = new JPanel();
		start();
	}
	public void run(){
		try {
			toUser.setLineWrap(true);
			toUser.setWrapStyleWord(true) ;
			toUser.setEditable(false);
			qScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS) ;
			qScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			mainPanel.add(qScroller);
			frame.setSize(400,400);
			frame.getContentPane().add(mainPanel);
			frame.setTitle("Server - TChatP");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setResizable(false);
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);
			serverSocket = new ServerSocket(port);
			while(true){ // every time a message as been sent
				Socket socket = serverSocket.accept();
				sockets.add(socket);
				new SenderUtil(socket, sockets, correctVersion, this);
			}
		}catch (IOException e) {
			System.err.println("Server already running, closing...");
			System.exit(0);
		}
	}
	public void display(String message){
		toUser.setText(toUser.getText() + message);
	}
	
	public ArrayList<String> getUserList(){
		return userList;
	}
}