import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Settings extends JFrame implements ActionListener{
	private String username, ip;
	private int port;
	private Client client;
	private JPanel panel;
	private JLabel titleLabel, usernameLabel, ipLabel, portLabel;
	private JButton cancel, confirm;
	private JTextField usernameArea, ipArea, portArea;
	private SpringLayout layout;
	public Settings(Client aClient){
		
		//Configuration
		
		client = aClient;
		username = client.getUsername();
		ip = client.getIp();
		port = client.getPort();
		titleLabel = new JLabel("Change the settings");
		usernameLabel = new JLabel("username:");
		ipLabel = new JLabel("ip:");
		portLabel = new JLabel("port:");
		usernameArea = new JTextField(15);
		ipArea = new JTextField(15);
		portArea = new JTextField(15);
		cancel = new JButton("Cancel");
		confirm = new JButton("Confirm");
		panel = new JPanel();
		layout = new SpringLayout();
		
		//Configuring the text and button action
		
		usernameArea.setText(username);
		ipArea.setText(ip);
		portArea.setText(Integer.toString(port));
		cancel.addActionListener(this);
		confirm.addActionListener(this);
		
		//Creating the panel and setting the layout
		
		panel.setLayout(layout);
		panel.add(titleLabel);
		panel.add(usernameLabel);
		panel.add(usernameArea);
		panel.add(ipLabel);
		panel.add(ipArea);
		panel.add(portLabel);
		panel.add(portArea);
		panel.add(cancel);
		panel.add(confirm);
		
		//Settings the constraints
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER,titleLabel,0,SpringLayout.HORIZONTAL_CENTER,panel);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER,titleLabel, 10,SpringLayout.NORTH,panel);
		
		//IP Layout Configuration
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER,ipArea,0,SpringLayout.HORIZONTAL_CENTER,panel);
		layout.putConstraint(SpringLayout.EAST,ipLabel,0,SpringLayout.WEST,portArea);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER,ipArea, -40,SpringLayout.VERTICAL_CENTER,panel);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER,ipLabel, -40,SpringLayout.VERTICAL_CENTER,panel);
		
		//Port Layout Configuration
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER,portArea,0,SpringLayout.HORIZONTAL_CENTER,panel);
		layout.putConstraint(SpringLayout.EAST,portLabel,0,SpringLayout.WEST,portArea);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER,portArea, -20,SpringLayout.VERTICAL_CENTER,panel);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER,portLabel, -20,SpringLayout.VERTICAL_CENTER,panel);
		
		//Username Layout Configuration
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, usernameArea, 0, SpringLayout.HORIZONTAL_CENTER,panel);
		layout.putConstraint(SpringLayout.EAST,usernameLabel,0,SpringLayout.WEST,usernameArea);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER,usernameArea, 0,SpringLayout.VERTICAL_CENTER,panel);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER,usernameLabel,0,SpringLayout.VERTICAL_CENTER,panel);
		
		//Cancel Button Layout Configuration
		layout.putConstraint(SpringLayout.EAST,cancel,-90,SpringLayout.EAST,panel);
		layout.putConstraint(SpringLayout.SOUTH,cancel, 0,SpringLayout.SOUTH,panel);
		
		//Confirm Button Layout Configuration
		layout.putConstraint(SpringLayout.EAST,confirm,0,SpringLayout.EAST,panel);
		layout.putConstraint(SpringLayout.SOUTH,confirm, 0,SpringLayout.SOUTH,panel);
		
		//frame setting
		
		getContentPane().add(panel);
		setResizable(false);
		setSize(500,500);
		addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent windowEvent){// acts like cancel button
				ipArea.setText(client.getIp());
				portArea.setText(Integer.toString(client.getPort()));
				usernameArea.setText( client.getUsername());
				setVisible(false);
			}
		});
		setLocationRelativeTo(null);
	}
	@Override
	public void actionPerformed(ActionEvent actionEvent){
		if(actionEvent.getSource() == cancel){
			ipArea.setText(client.getIp());
			portArea.setText(Integer.toString(client.getPort()));
			usernameArea.setText(client.getUsername());
			this.setVisible(false);
		}else if(actionEvent.getSource() == confirm){
			client.setIp(ipArea.getText());
			client.setPort(Integer.parseInt(portArea.getText()));
			client.setUsername(usernameArea.getText());
			this.setVisible(false);
		}
	}
}
