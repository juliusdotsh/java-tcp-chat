import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class SenderUtil extends Thread{
	private boolean isClient;
	private Socket clientSocket;
	private ArrayList<Socket> sockets = new ArrayList<>();
	private int version;
	private Server server;
	private Client client;
	private BufferedReader in;
	private String s;
	private int userPosition;
	private static final String CODE = "\u00A7";
	private static final int OK = 0;
	private static final int GENERIC_ERROR = 2;
	private static final int VERSION_NOT_EXISTS = 3;
	private static final int MESSAGE = 7;
	private static final int USER_JOIN = 9;
	private static final int USER_LEAVE = 10;
	private static final int CONNECT = 11;
	public SenderUtil(Socket aSocket, ArrayList<Socket> aSocketList,int theVersion,Server aServer){//SERVER
		isClient = false;
		clientSocket = aSocket;
		sockets = aSocketList;
		version = theVersion;
		server = aServer;
		start();
	}
	public SenderUtil(Socket socket,int aVersion, Client aClient){//CLIENT
		isClient = true;
		clientSocket = socket;
		version = aVersion;
		client = aClient;
		start();
	}
	public void run(){
		if(isClient){
			send(Integer.toString(version));
			send(CONNECT+CODE+client.getUsername()+CODE);
			send(USER_JOIN+CODE+client.getUsername());
		}else{
			try{
				in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), "UTF-8"));
				for(Socket sock: sockets) {
					while ((s = in.readLine()) != null){
						if(!s.contains(CODE)){
							checkVersion(version);
						}else{
							int typeOfMessage = Integer.parseInt(s.split(CODE)[0]);
							BufferedWriter out;
							switch(typeOfMessage){
								case USER_LEAVE:
									out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream(), "UTF-8"));
									out.write(USER_LEAVE+CODE+server.getUserList().get(userPosition));
									out.newLine();
									out.flush();
									server.display(server.getUserList().get(userPosition)+" disconnected.\n");
									break;
								case CONNECT:
									if(sock.equals(clientSocket)){
										out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream(), "UTF-8"));
										String[] messageDivided = s.split(CODE);
										System.out.println(messageDivided[1]);
										if(!server.getUserList().contains(messageDivided[1]+"")){
											server.getUserList().add(messageDivided[1]+"");
											userPosition = messageDivided.length - 1;
										}
										out.write(OK+CODE+printUserList());
										out.newLine();
										out.flush();
									}
									out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream(), "UTF-8"));
									out.write(USER_JOIN+CODE+s.split(CODE)[1]);
									out.newLine();
									out.flush();
									break;
									
								case MESSAGE:
									String[] messagepart = s.split(CODE);
									out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream(), "UTF-8"));
									out.write(s);
									out.newLine();
									out.flush();
									server.display(messagepart[2] +":" +messagepart[1] + '\n');
									break;
							}
							
						}
					}
				}
				clientSocket.close();
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}
	public void send(String message){
		try{
			PrintWriter outToServer = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(), "UTF-8"));
			outToServer.println(message);
			outToServer.flush();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public void checkVersion(int version){
		try{
			int t = Integer.parseInt(s);
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream(), "UTF-8"));
			if( t == version) {
				out.write(OK+"");
				out.newLine();
				out.flush();
			}else if(t > version){
				out.write(VERSION_NOT_EXISTS+"");
				out.newLine();
				out.flush();
			}else{
				out.write(GENERIC_ERROR+CODE+"You are using the old version, use the "+ version + " version");
				out.newLine();
				out.flush();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public String printUserList(){
		ArrayList<String> users = server.getUserList();
		String printedArray = "";
		for(String user: users){
			printedArray = printedArray + user + "§";
		}
		return printedArray;
	}
}
