import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Chooser extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	JPanel panel = new JPanel();
	JButton client = new JButton();
	JButton server = new JButton();
	JLabel label = new JLabel();
	public Chooser(){
		getContentPane().add(panel);
		SpringLayout layout = new SpringLayout();
		panel.setLayout(layout);
		client.setText("Client");
		client.addActionListener(this);
		server.addActionListener(this);
		server.setText("Server");
		label.setText("Select what do you whant to do");
		panel.add(client);
		panel.add(server);
		panel.add(label);
		//Client Button layout
		layout.putConstraint(SpringLayout.WEST, client, 50, SpringLayout.WEST, panel);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, client, 0, SpringLayout.VERTICAL_CENTER, panel);
		//Server Button layout
		layout.putConstraint(SpringLayout.EAST, server, -50, SpringLayout.EAST, panel);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, server, 0, SpringLayout.VERTICAL_CENTER, panel);
		//label Button layout
		layout.putConstraint(SpringLayout.NORTH, label, 20, SpringLayout.NORTH, panel);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, label,  0 , SpringLayout.HORIZONTAL_CENTER, panel);
		setTitle("TChatP");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300,150);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == client){
			setVisible(false);
			new Client();
		}else if(e.getSource() == server){
			setVisible(false);
			new Server();
		}
	}
}
